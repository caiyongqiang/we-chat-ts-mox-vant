
## 用法

```
// params传参
httpRequest.get<T>(baseUrl + "/xxx", params )

// data传参
httpRequest.post<T>(baseUrl + "/xxx", data)

// 其它传参更换get/post即可
```

### 创建接口
* 发起请求引入`utils/request`
* 上传文件引入`utils/uploadFile`

```
import { httpRequest } from '../../utils/request'
const baseUrl = require('../base').allBaseUrl.GDEnvs.host

export default class userApi {
  /**
   * @description: 获取用户信息
   * @return {*}
   */
  static getUserInfo = (data: Object) =>
    httpRequest.post<userInfo>(
      baseUrl + '/serviceProduct/showcase/getShowcase',
      data,
      // {header: {}, noShowMsg: false} /**不填，取默认值*/
    )
}
```

### 页面使用

```
import $api from '../../api/index'

$api.userApi.getUserInfo({}).then((res:any) => {
  console.log(res, 'userApi')
}).catch((err:any) => {
  console.err(err)
})
```

## 目录说明
system存放全局通用api
business目录存放业务api
