/*
 * @Author: caiyongqiang
 * @Date: 2022-10-20 10:46:54
 * @LastEditTime: 2022-10-20 16:58:01
 * @LastEditors: caiyongqiang
 * @Description: 
 */

import userApi from './system/userApi'
import uploads from "./system/uploads"

class apis {
  /**
   * @description: 用户相关Api
   */
  static userApi = userApi
  /**
   * @description: 上次图片接口
   */
   static uploads = uploads
}

export default apis
