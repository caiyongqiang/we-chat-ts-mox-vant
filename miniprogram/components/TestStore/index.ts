/*
 * @Author: xxx
 * @Date: 2022-10-19 22:08:27
 * @LastEditTime: 2023-03-28 11:39:20
 * @LastEditors: xxx
 * @Description: 测试案例
 */
import { ComponentWithStore } from 'mobx-miniprogram-bindings'
import { globalStore } from '../../store/index'
ComponentWithStore({
  options: {
    styleIsolation: 'shared'
  },
  data: {
    someData: '...'
  },
  storeBindings: {
    store: globalStore,
    fields: ["numA", "numB", "sum"],
    actions: {
      buttonTap: "update",
    },
	},
	methods: {
	}
})
