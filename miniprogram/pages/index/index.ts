/*
 * @Author: xxx
 * @Date: 2022-10-18 16:46:07
 * @LastEditTime: 2022-10-27 10:02:33
 * @LastEditors: xxx
 * @Description: 
 */
import { ComponentWithStore } from 'mobx-miniprogram-bindings'
import { storeBindings } from "./behavior"
import { userStore } from "../../store/index"
import $api from '../../api/index'

// const app = getApp<IAppOption>()

ComponentWithStore({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName'), // 如需尝试获取用户信息可改为false
    checked: true,
    villageList: [] as Village[]
  },
  ...storeBindings,
  methods: {
    // 事件处理函数
    bindViewTap() {
      wx.navigateTo({
        url: '../logs/logs',
      })
    },
    onLoad() {
      // @ts-ignore
      if (wx.getUserProfile) {
        this.setData({
          canIUseGetUserProfile: true
        })
      }
    },
    /**测试发起请求*/
    onAdd() {
      $api.userApi.getUserInfo({ username: 'demo', password: '123456' }).then((res) => {
        if (res.code === 200) {
          userStore.setUserInfo(res.data.userInfo)
        }
      })
      userStore.increase(1)
      // @ts-ignore
      // this.increase(1)
    },
    bindGetVillage() {
      $api.userApi.getVillageList().then((res) => {
        if (res.code === 200) {
          const list = res.data.list
          this.setData({ villageList: list })
        }
      })
    },
    /**测试上传图片*/
    onUpladImage() {
      wx.chooseImage({
        count: 9,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success: (result) => {
          let tempFilePaths = result.tempFilePaths[0]
          $api.uploads.uploadImg(tempFilePaths).then(res => {
            // console.log(res.data, '测试输出')
          })
        },
        fail: () => { },
        complete: () => { }
      });
    },

    // VR跳转
    bindVR() {
      wx.navigateTo({url: "/VRModule/VR/index"})
    },


    getUserProfile() {
      // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
      wx.getUserProfile({
        desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          console.log(res)
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    },
    getUserInfo(e: any) {
      // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
      console.log(e)
      this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })
    }
  }
})
