/*
 * @Author: xxx
 * @Date: 2022-10-26 10:54:36
 * @LastEditTime: 2022-10-26 18:32:06
 * @LastEditors: xxx
 * @Description: 
 */
import { ComponentWithStore } from 'mobx-miniprogram-bindings'
import { globalStore } from '../../store/index'
import Painter from './painter'
const painter = new Painter()
ComponentWithStore({
  data: {
    a: 1,
    b: 3,
    sum: 0
  },
  storeBindings: [
    {
      store: globalStore,
      fields: {
        numA: (store: any) => store.numA,
        // numB: (store) => store.numB,
        // sum: (store) => store.sum,
      },
      actions: {
        updataNum: "update"
      },
    }],
  observers: {
    "a,b": function (a,b) {
      this.setData({sum: a + b})
    },
    "a, numA": function (a,numA) {
      this.setData({sumStore: numA + a})
    }
  },
  methods: {
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
    },

    // 海报生成
    bindPoster() {
      this.setData({template: painter.palette()})
    },
    // 海报生成输出
    onImgOK(e:any) {
      wx.previewImage({
        current: e.detail.path,
        urls: [e.detail.path],
      });
    },

    // 改变data内容监听
    bindUpdata() {
      this.setData({ a: this.data.a + 2, b: this.data.b + 1 })
    },

    /** 开始图片裁剪 */
    bindCropper() {
      const that = this
      wx.chooseImage({
        count: 1,
        sizeType: ['original','compressed'],
        sourceType: ['album','camera'],
        success: (result)=>{
          const src = result.tempFilePaths[0]
          that.selectComponent('#imgCorpprt').initOpenCropper(src)
        },
        fail: ()=>{},
        complete: ()=>{}
      });
    },
    /** 接受图片裁剪完成地址 */
    onCorpperOk(e: any) {
       console.log(e.detail.path, 'path')
       const path = e.detail.path
       wx.previewImage({
        current: '',
        urls: [path],
       });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage(e: any) {
    }
  },
})