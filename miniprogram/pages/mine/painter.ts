/*
 * @Author: xxx
 * @Date: 2022-10-26 18:20:40
 * @LastEditTime: 2022-10-26 18:35:57
 * @LastEditors: xxx
 * @Description: 
 */
export default class Painter {
  palette() {
    return {
      width: '750rpx',
      height: '1334rpx',
      background: 'https://qiniu-image.qtshe.com/20190506share-bg.png',
      views: [
        {
          type: 'image',
          url: 'https://qiniu-image.qtshe.com/1560248372315_467.jpg',
          css: {
            top: '32rpx',
            left: '30rpx',
            right: '32rpx',
            width: '688rpx',
            height: '420rpx',
            borderRadius: '16rpx'
          },
        },
        {
          type: 'image',
          url: wx.getStorageSync('avatarUrl') || 'https://img2.baidu.com/it/u=2072574549,3761143786&fm=253&fmt=auto&app=138&f=JPEG?w=450&h=450',
          css: {
            top: '404rpx',
            left: '328rpx',
            width: '96rpx',
            height: '96rpx',
            borderWidth: '6rpx',
            borderColor: '#FFF',
            borderRadius: '96rpx'
          }
        },
        {
          type: 'text',
          text: wx.getStorageSync('nickName') || '哈罗哈皮',
          css: {
            top: '532rpx',
            fontSize: '28rpx',
            left: '375rpx',
            align: 'center',
            color: '#3c3c3c'
          }
        },
        {
          type: 'text',
          text: `邀请您参与助力活动`,
          css: {
            top: '576rpx',
            left: '375rpx',
            align: 'center',
            fontSize: '28rpx',
            color: '#3c3c3c'
          }
        },
        {
          type: 'text',
          text: `宇宙最萌蓝牙耳机测评员`,
          css: {
            top: '644rpx',
            left: '375rpx',
            maxLines: 1,
            align: 'center',
            fontWeight: 'bold',
            fontSize: '44rpx',
            color: '#3c3c3c'
          }
        },
        {
          type: 'image',
          url: 'https://img.yzcdn.cn/vant-weapp/qrcode-201808101114.jpg',
          css: {
            top: '834rpx',
            left: '470rpx',
            width: '200rpx',
            height: '200rpx'
          }
        }
      ]
    }
  }
}