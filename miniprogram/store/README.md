
# 文档说明
* 1、小程序的 MobX 绑定辅助库，用于全局状态管理
* 2、按模块区分，可以自己创建文件，统一导出到index.ts文件中
* 3、[文档地址](https://github.com/wechat-miniprogram/mobx-miniprogram-bindings)
* 4、文档介绍说明，`Page 构造器适用于简单的页面。但对于复杂的页面， Page 构造器可能并不好用。`，所以所以页面或者组件统一使用 `Component` 构造器，这里使用`ComponentWithStore`构造器，方便全局状态数据读取。[Component 构造器构造页面，详情](https://developers.weixin.qq.com/miniprogram/dev/framework/app-service/page.html#%E4%BD%BF%E7%94%A8-Component-%E6%9E%84%E9%80%A0%E5%99%A8%E6%9E%84%E9%80%A0%E9%A1%B5%E9%9D%A2)

# 创建模块
```
import { observable, action } from 'mobx-miniprogram'

export const userStore = observable({
	// 定义响应式的属性
  count: 0,
  
  // 计算属性
  // get double () {
  //   return this.count * 2
  // },

  // action,修改store中的属性
  // 由于mobx-miniprogram库问题，我们需要加多一个this类型说明，不影响传入的参数
  increase: action(function (this: any, value:number) {
    this.count += value
  })
})
```

# 使用方法

### 在公用 `.ts`	文件中使用（此方法也适合页面、组件中使用）
* 此方法可以避免ts语法检查错误(例如：页面直接使用this.youMethods，会提示youMethods未定义)
```
	<!-- 引入store方法 -->
	import {userStore} from '../store/index'
	<!-- 直接使用自定义的方法 -->
	userStore.youMethods()
```

### 在 `Component` 构造器中使用：

```
<!-- 导入必要方法 -->
import { ComponentWithStore } from 'mobx-miniprogram-bindings'
<!-- 导入自己写的store文件 -->
import { global } from '../../models/index'
ComponentWithStore({
  data: {
    someData: '...'
  },
  storeBindings: [{ // 数组对象形式，可以导入多个store
    store: global,
    fields: ["numA", "numB", "sum"],
    actions: {
      buttonTap: "update", // buttonTap直接在.wxml标签中命名
    },
  }],
	methods: { // 组件方法
	}
})
```

# 注意事项
* 延迟更新与立刻更新
* 为了提升性能，在 store 中的字段被更新后，并不会立刻同步更新到 this.data 上，而是等到下个 wx.nextTick 调用时才更新。（这样可以显著减少 setData 的调用次数。）

* 如果需要立刻更新，可以调用：
* `this.updateStoreBindings()` （在 behavior 绑定 中）
* `this.storeBindings.updateStoreBindings()` （在 手工绑定 中）

## 与 miniprogram-computed 一起使用
* 与 miniprogram-computed 时，在 behaviors 列表中 computedBehavior 必须在后面：
```
Component({
  behaviors: [storeBindingsBehavior, computedBehavior],
  /* ... */
});
```

# 关于部分更新
* 如果只是更新对象中的一部分（子字段），是不会引发界面变化的！例如：
```
Component({
  behaviors: [storeBindingsBehavior],
  storeBindings: {
    store,
    fields: ["someObject"],
  },
});
```
* 如果尝试在 store 中：
* `this.someObject.someField = "xxx";`
* 这样是不会触发界面更新的。请考虑改成：
* `this.someObject = Object.assign({}, this.someObject, { someField: "xxx" });`