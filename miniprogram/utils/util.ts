export const formatTime = (date: Date) => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return (
    [year, month, day].map(formatNumber).join('/') +
    ' ' +
    [hour, minute, second].map(formatNumber).join(':')
  )
}

const formatNumber = (n: number) => {
  const s = n.toString()
  return s[1] ? s : '0' + s
}

/**
 * @des 基础库版本号比较,例如：2.10.0
 * @param v1 当前小程序基础库小程序
 * @param v2 要比较的基础库版本
 * @return -1||0||1 // 当-1: v1 < v2  当0: v1 = v2  当1: v1 > v2
 */
export const compareVersion = (SDKversion1: string, SDKversion2: string) => {
  const v1 = SDKversion1.split('.')
  const v2 = SDKversion2.split('.')
  const len = Math.max(v1.length, v2.length)

  while (v1.length < len) {
    v1.push('0')
  }
  while (v2.length < len) {
    v2.push('0')
  }

  for (let i = 0; i < len; i++) {
    const num1 = parseInt(v1[i])
    const num2 = parseInt(v2[i])

    if (num1 > num2) {
      return 1
    } else if (num1 < num2) {
      return -1
    }
  }

  return 0
}
