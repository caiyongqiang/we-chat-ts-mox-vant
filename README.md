<!--
 * @Author: xxx
 * @Date: 2022-10-20 17:45:44
 * @LastEditTime: 2023-03-21 17:46:45
 * @LastEditors: xxx
 * @Description: 项目说明
-->
## git使用规范
* 1、git clone http://gitxxxx.com
* 2、创建一个本地分支关联自己的开发远程分支
      git checkout -b dev_xxx origin/dev_xxx
* 3、仅更新本地代码步骤：
    - 1)拉取dev分支最新代码
        - `git pull -r origin dev`
    - 2)合并dev分支。(此步骤可能遇到代码冲突，解决代码冲突后提交一次即可。)
        - `git rebase origin/dev`
        - `git pull`
    （此时本地的开发分支dev_xxx就同步了dev主分支的最新代码。）
* 4、开发完提交代码步骤：
    - 1)提前安装vscode插件：GitLens—Gitsupercharged插件。
       - 在插件中先暂存修改的代码，填写提交消息，再点击提交。即完成了代码的commit。
    - 2)拉取dev分支最新代码
       - `git pull -r origin dev`
    - 3)在自己的开发分支，合并dev分支
       - `git rebase origin/dev`
       - `git pull`
    **（提示：如果此时有代码冲突，需要先解决代码冲突，才能push）**
    - 4)提交代码，推送本地开发分支到远程开发分支
       - `git push`
    - 5)发起合并请求（把远程开发分支dev_xxx上修改的内容合并到远程主分支dev）
#### 注意 git合并请求填写请求内容需要填写的项：
  * a.title简明扼要的修改模块描述
  * b.Description详细细致描述修改内容
  * c.label表明本次提交的类型，可多选

## 开发工具IDE规范（统一编辑器可提高效率、减少代码冲突）
* 1、建议使用`vscode`编辑器开发
* 2、统一把tab缩进符改为`2格`
* 3、vscode可以安装下面最基本的插件：
    - `ESLint`                    （代码检查工具）
    - `GitLens`                   （git工具即可上传代码）
    - `Prettier - Code formatter` （代码格式化）
    - `WXML - Language Service`   （微信小程序 .wxml 文件代码高亮，标签、属性的智能补全）
    - `wechat-snippet`            （微信小程序代码辅助,代码片段自动完成）

## 运行注意事项
* 1、第一次拉取代码后`cd we-chat-ts-mox-vant` 然后运行 `npm install`
* 2、如果新增npm或者没有生成miniprogram_npm文件包，请在`微信开发工具`界面=>左上角`工具`=>点击`构建npm`即可
* 3、如果预览或者上传报语法错误，请在`微信开发工具`界面=>右上角`详情`=>点击`本地设置`，勾选`将js编译成ES5`

## 使用的插件或者npm说明
#### 全局状态管理
  * npm名称：mobx-miniprogram && mobx-miniprogram-bindings
  * 文档/git：[git文档详情](https://github.com/wechat-miniprogram/mobx-miniprogram-bindings)
  * 说明：因为使用上面2个组件，不能和`miniprogram-computed`组件的ts版本暂时无法兼容，故舍弃`miniprogram-computed`，[git详情](https://github.com/wechat-miniprogram/computed)；如需使用watch功能，建议使用微信自带属性[observers](https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/observer.html)
#### UI库
  * UI库名称：Vant Weapp
  * 文档: [文档详情](https://vant-contrib.gitee.io/vant-weapp/#/home)
#### Painter 海报生成工具
  * 组件名称：Painter
  * 文档/git：[git详情](https://github.com/Kujiale-Mobile/Painter)
  * 说明：本插件用于海报生成，组件支持px和rpx单位，已经全局（app.json)导入，页面使用标签即可
  ```
  <Painter style="position: absolute; top: -9999rpx;" palette="{{template}}" bind:imgOK="onImgOK"></Painter>
  ```
#### WeCropper 裁剪图片工具
  * 组件名称：WeCropper
  * 文档/git: [git详情](https://github.com/we-plugin/we-cropper), [文档说明及api详情](https://we-plugin.github.io/we-cropper/#/?id=%e5%be%ae%e4%bf%a1%e5%b0%8f%e7%a8%8b%e5%ba%8f%e5%9b%be%e7%89%87%e8%a3%81%e5%89%aa%e5%b7%a5%e5%85%b7we-cropper)
  * 说明：此组件已经进行二次封装，路径components/ImgCropper/index(如需引入，请查看该该文件说明)
    * 本组件原文件修改过2个bug（已在文件中修复）
    * 1、不支持引入组件中使用
    * 2、canvas-2d 拖拽会出现图片重影问题


## 开发规范
* 1、ts声明文件规范：统一在typings=>types=>business|system创建文件声明，可自行创建文件
* 2、组件命名规范：文件名必须`大写开头，驼峰命名法`，如MyComponent
    - 组件或页面文件，文件夹下面的子文件统一使用`index`命名，
    - 例如 MyComponent
        - index.wxml
        - index.json
        - index.ts
        - index.scss
* 3、api使用规范：业务接口统一在`business`文件夹里面创建，系统或者公用接口在`system`,对应第1点ts声明文件
* 4、命名规范：
    - js函数名、class名统一使用`驼峰命名法`，命名需要有`特殊含义`名称太长可取文字首字母，如广州市天河区人口总量：GZSTHQPersonCount
    - 页面事件方法用`bind`开头(bindUpUserInfo)，父子组件传值方法用`on`开头(onTabTitle)，其它普通方法用常用单词开头(set、get、open、close、jump)
* 5、组件引入规范：组件路径引入(即.json文件)，统一使用绝对路径（如：/components/TestStore/index）
* 6、测试mock数据，可以使用微信开发工具模拟，[点击查看详情](https://developers.weixin.qq.com/miniprogram/dev/devtools/api-mock.html#API-Mock)
* 7、页面或组件统一使用`ComponentWithStore`构造器，因为`Page 构造器适用于简单的页面`
* 8、如果组件是全局使用，可在app.json中`usingComponents`字段引入组件，全局可用

# 感谢
项目中VR部分由[Edison](https://gitee.com/threejs/three-weixin-demo)提供学习，仅供学习，如算抄袭或者不可用请通知我，自行删除

## 文档结构说明
```
├─.eslintrc.ts
├─package-lock.json
├─package.json                // npm构建版本
├─project.config.json
├─project.private.config.json
├─README.md                   // 项目说明文档
├─tsconfig.json               // ts配置文件
├─typings                     // ts声明文件
|    ├─index.d.ts
|    ├─types
|    |   ├─index.d.ts
|    |   ├─wx                 // 微信api等ts声明
|    |   ├─business           // 业务相关自定义声明文件
|    |   ├─system             // 系统相关自定义声明文件
|    |   |   └user.d.ts
├─miniprogram                 // 小程序代码部分
|      ├─app.json
|      ├─app.scss
|      ├─app.ts
|      ├─sitemap.json
|      ├─utils                // 公用的ts文件
|      |   ├─request.ts       // http服务器请求封装
|      |   ├─uploadFile.ts    // http上传请求封装
|      |   └util.ts
|      ├─style                // 公共style样式，可自定义
|      |   └base.scss
|      ├─store                // 全局状态管理文件
|      |   ├─globalStore.ts
|      |   ├─index.ts
|      |   ├─README.md        // stroe使用说明
|      |   └userStore.ts
|      ├─pages                // 页面代码
|      |   ├─logs
|      |   |  ├─logs.json
|      |   |  ├─logs.ts
|      |   |  ├─logs.wxml
|      |   |  └logs.wxss
|      |   ├─index
|      |   |   ├─behavior.ts
|      |   |   ├─index.json
|      |   |   ├─index.scss
|      |   |   ├─index.ts
|      |   |   └index.wxml
|      ├─components           // 公共组件代码
|      |     ├─TestStore
|      |     |     ├─TestStore.json
|      |     |     ├─TestStore.scss
|      |     |     ├─TestStore.ts
|      |     |     └TestStore.wxml
|      ├─api                  // 全局api接口文件
|      |  ├─base.ts           // 全部接口域名封装，支持封装多组域名
|      |  ├─index.ts          // 接口统一导出文件
|      |  ├─README.md         // api的调用使用说明
|      |  ├─system            // 系统通用接口文件
|      |  |   ├─uploads.ts
|      |  |   └userApi.ts
|      |  ├─business           // 业务接口
```