/*
 * @Author: caiyongqiang
 * @Date: 2022-10-20 15:06:24
 * @LastEditTime: 2022-10-25 15:56:51
 * @LastEditors: caiyongqiang
 * @Description: 
 */
/**
 * @description: 提交登录的数据
 */
declare interface UserInfo {
  username: string
  password: string
  type?: string
  key?: string
  code?: string
  source?: string
  state?: string
  phone?: string
}
 declare interface ReturnUserInfo {
  userInfo: UserInfo
}

declare interface Village {
  icon: string,
  industryName: string,
  id: string,
  img: string,
  industryContent: string
}
declare interface VillageList {
  list: Village[]
}