
interface Options {
	x: Number 		// 裁剪框x轴起点（默认 0）
	y: Number 		// 裁剪框y轴起点（默认 0）
	width: Number // 裁剪框宽度（默认 画布宽度），单位：px
	height: Number //裁剪框高度（默认 画布高度），单位：px
}
interface BoundStyle {
	color: String    		// 裁剪框颜色（默认 #04b00f）
	lineWidth: Number   // 裁剪框线条宽度（默认 1）
	mask: String 				// 遮罩层颜色（默认 rgba(0, 0, 0, 0.3)）
}

declare interface CropperOpt {
	id: string 						// 用于手势操作的canvas组件标识符（必填）
	targetId: string			// 用于生成截图的canvas组件标识符（必填）
	pixelRatio: number		// 设备像素比 （必填）
	canvas?: object				// 自定义画布，当使用 canvas2d 时必填
	component_this?: object				// 页面this，当组件引入封装组件时必填 --- 二次开发增加字段
	ctx?: Object					// 自定义画布上下文，可用于 canvas2d
	targetCtx?: object		// 自定义画布上下文，可用于 canvas2d
	src?: string 					// 需裁剪的图片资源
	width?: number 				// 容器宽度，单位：px
	height?: number 			// 容器高度，单位：px
	scale?: number 				// 最大缩放倍数
	zoom?: number 				// 缩放系数
	cut: Options 				// 裁剪框
	boundStyle?: BoundStyle
	onReady?: () => void
}

interface CanvasToOpt {
	// c_this?: Object				// 二次开发封装组件时必填，2d不用传
	original?: Boolean 		// 是否使用原图模式（默认值 false）
	quality?: Number 			// 图片的质量，目前仅对 jpg 有效。取值范围为 (0, 1]，不在范围内时当作 1.0 处理。
	fileType?: String 		// 目标文件的类型 v1.3.3支持
	componentContext?: Object  // 在自定义组件下，当前组件实例的this，以操作组件内 <canvas> 组件
}

declare interface Cropper {
	updateCanvas: () => void					// 更新画布视图
	pushOrign: (src: string) => void	// 载入图片
	removeImage: () => void					// 清除图片
	getCropperImage: any //  裁剪后的图片路径
	getCropperBase64: () => { base64: string, err?: { message: string } }	// 裁剪后的图片（base64编码）
	touchStart: (e: object) => void		// 接收（手指触摸动作开始）事件对象
	touchMove: (e: object) => void			// 接收（手指触摸后移动）事件对象
	touchEnd: (e: object) => void			// 接收（手指触摸后移动）事件对象
}
